# -*- coding: utf-8 -*-
import logging

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.contrib.auth.models import User

logger = logging.getLog(__name__)

class Command(BaseCommand):
    help = "Init DEBUG data base before syncdb"

    def handle(self, *args, **options):
        dev_email = getattr(settings, 'DEV_EMAIL', 'devel@suvit.ru')
        dev_pass = getattr(settings, 'DEV_PASS', 'sha1$77471$655d6f6bf877ea83d1717016aca8e2e5b6ddaed8')
        # Заменяем емейлы пользователей и пароль (по умолчанию - 1)
        User.objects.all().update(email=dev_email,
                                  password=dev_pass)
        logger.info('"auth_user " updated')
