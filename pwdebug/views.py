import time

from django.views.generic.base import RedirectView
from django.contrib.admin.views.decorators import staff_member_required


def test_error(request):
    raise Exception('Error passed')


@staff_member_required
def test_long_request(request):
    time.sleep(60)
    return RedirectView.as_view(url='/')(request)
