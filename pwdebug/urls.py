# -*- coding: utf-8 -*-
from django.conf.urls.defaults import patterns

urlpatterns = patterns('pwdebug.views',
    (r'^debug/error/$', 'test_error', {}, 'debug_error'),
    (r'^debug/long_request/$', 'test_long_request', {},
      'debug_long_request'),

)