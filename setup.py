# -*- coding: utf-8 -
#
# This file is part of django-pwdebug released under the MIT license. 
# See the LICENSE for more information.

import os
import sys
from setuptools import setup, find_packages

setup(
    name='django-pwdebug',
    version=__import__('pwdebug').VERSION,
    description='Django specific debug utils and helpers',
    long_description=file(
        os.path.join(
            os.path.dirname(__file__),
            'README.md'
        )
    ).read(),
    author='Suvit Org',
    author_email='mail@suvit.ru',
    license='MIT',
    url='http://bitbucket.org/suvitorg/django-pwdebug',
    zip_safe=False,
    packages=find_packages(exclude=['docs', 'examples', 'tests']),
    include_package_data=True,
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Framework :: Django',
        'License :: MIT License',
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Environment :: Web Environment',
        'Topic :: Software Development',
    ]
)
